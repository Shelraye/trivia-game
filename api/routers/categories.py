
from pydantic import BaseModel
from fastapi import APIRouter, Depends
from db import CategoriesQueries

router = APIRouter()

class CategoriesOut(BaseModel):
    id: int 
    title: str
    cannon: str

class CategoryOut(BaseModel):
    categories: list[CategoriesOut]

@router.get("/api/categories")
def categories_list(queries: CategoriesQueries = Depends()):
    return {
        "categories" : queries.get_all_categories(),
    }

