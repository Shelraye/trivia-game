import os
from psycopg2 import ConnectionPool

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])

class CategoriesQueries:
    def get_all_categories(self):
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                SELECT id, title, cannon
                FROM categories
                ORDER BY ascending
                """)

                results = []
                for row in cur.fetchall():
                    record = {}
                    for i, column in enumerate(cur.description) :
                        record[column.name] = row [i]
                    results.append(record)
                return results